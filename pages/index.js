//File handeling the content page
import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout, { siteTitle } from '../components/layout';
import utilStyles from '../public/styles/utils.module.css';


export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <h1 className={utilStyles.headingXl}>
        Hello and welcome to my website. I wish you a fun time exploring!
      </h1>
      <h1 className={utilStyles.headingLg}>Here you find some stuff I'm working on right now:</h1>
      <div className={utilStyles.cardpic}>
        <h2>Unity Robotics Hub</h2> 
        <div className={utilStyles.imgg}>
          <Image 
            src="/../public/spawn_bricks.gif"
            layout = "intrinsic"
            height= "400"
            width= "700"
            alt="some stuff"
          />
        </div>
        <div className={utilStyles.imgg}>
          <Image 
            src="/../public/inverse_Kinematics.gif"
            layout = "intrinsic"
            height= "400"
            width= "700"
            alt="some stuff"
          />
        </div>
        <p> 
            This semester I was part of a project team where we created an "Augmented-Reality-App". We used the <Link href="https://github.com/lastthought15/Unity-Robotics-Hub"
            ><a>Unity-Robotics-Hub repository</a></Link> as a basis, to develop a simple building task. The hole project was to showcase the integration 
            of different repositories to solve a problem statement as a Proof-of-Concept. The finished work can be found in this <Link href="https://github.com/wolfboyyang/Unity-Robotics-Hub">
            <a>repo</a></Link>. My contribution focused on the ROS part. If you are 
            interested you can check out my work <Link href="https://github.com/lastthought15/Unity-Robotics-Hub"><a>here</a></Link>.
        </p>
      </div>
      <div className={utilStyles.cardpic}>
        <h2>Next.js</h2>
        <p>
            This Page was generated using the <strong>next.js framework</strong>. It was developed for buidling fast web applications.
            It is based on <strong>React</strong> a Javascript library for building interactive user interfaces.
            If you want to learn more about next.js, follow this <Link href="https://nextjs.org/learn/foundations/about-nextjs/what-is-nextjs"><a>link</a></Link>.
            I used this <Link href="https://github.com/vercel/next-learn/blob/master/basics/learn-starter/pages/index.js"><a>template</a></Link> as the startpoint for my site. 
            If you want to clone or fork my site you can find the repo <Link href="https://gitlab.com/rwth-crmasters-sose22/shared-runner-group/david_lukert_ssgsite"><a>here</a></Link>.
        </p>
        <p>
            Although this site was developed with a framework based on high user interaction it was deployed for a static-site-generator (SSG). 
            This allows you to host your site Platforms like GitLab, without the need to understand server and hosting structure. If you want to learn more about SSGs follow 
            this <Link href="https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/"><a>link</a></Link>. 
        </p>
        <p>
            In the future I want to build a dynamic website using next.js and react. 
        </p>
      </div>
      <div className={utilStyles.cardpic}>
        <h2>Creative Coding</h2>
        <div className={utilStyles.imgg}>
          <Image 
            src="/../public/creativecodd.png"
            layout = "intrinsic"
            height= "500"
            width= "500"
            alt="creative coding example"
          />
        </div>
        <div className={utilStyles.imgg}>
          <Image 
            src="/../public/creativecodgr.png"
            layout = "intrinsic"
            height= "500"
            width= "500"
            alt="creative coding example"
          />
        </div>
        <p>
        Recently I made a course on <Link href="https://www.domestika.org/en/courses/2729-creative-coding-making-visuals-with-javascript/course"><a>Domestika</a></Link>, that introduces creative coding using 
        the <Link href="https://github.com/mattdesl/canvas-sketch"><a>canvas-sketch</a></Link> libary for <strong>javascript</strong>. There are alomost no boundaries for what can be done. My first project can be
        found <Link href="https://github.com/lastthought15/Creative-Coding-nightcities"><a>here</a></Link>. Currently it's still in the set-up phase.
        </p>
      </div>
    </Layout>
  );
}